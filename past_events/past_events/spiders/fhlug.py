# -*- coding: utf-8 -*-
import scrapy

from past_events.items import PastEventItem


class FhlugSpider(scrapy.Spider):
    name = "fhlug"
    allowed_domains = ["fhlug.at"]
    start_urls = (
        'http://fhlug.at/category/talks/',
    )

    def parse(self, response):
        # Get new links to follow
        for url in response.xpath(
                '//a[@class="page-numbers"]/@href').extract():
            yield scrapy.Request(url)

        # Extract items
        for selection in response.xpath('//article'):
            item = PastEventItem()
            item['title'] = self._extract_title(selection)
            item['date'] = self._extract_date(selection)
            yield item

    def _extract_title(self, selection):
        return selection.xpath('header/h2/a/text()').extract()[0].strip()

    def _extract_date(self, selection):
        return selection.xpath(
            'div[@class="entry-content"]//text()').re('Wann:\s*(.*)')

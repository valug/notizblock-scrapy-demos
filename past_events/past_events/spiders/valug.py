# -*- coding: utf-8 -*-
import scrapy

from past_events.items import PastEventItem


class ValugSpider(scrapy.Spider):
    name = "valug"
    allowed_domains = ["valug.at"]
    start_urls = (
        'http://www.valug.at/Index/Vergangene%20Veranstaltungen',
    )

    def parse(self, response):
        for selection in response.xpath('//tr[not(td[1]/p/strong)]'):
            item = PastEventItem()
            item['title'] = self._extract_title(selection)
            item['date'] = self._extract_date(selection)
            item['count'] = self._extract_count(selection)
            yield item

    def _extract_title(self, selection):
        return selection.xpath('td[1]//text()').extract()[0].strip()

    def _extract_date(self, selection):
        return selection.xpath('td[2]//text()').extract()[0].strip()

    def _extract_count(self, selection):
        try:
            return int(selection.xpath('td[3]//text()').extract()[0].strip())
        except ValueError:
            return None

# -*- coding: utf-8 -*-

import scrapy

from lugs.loader import LugOrgUkItemLoader


class LugOrgUkSpider(scrapy.Spider):
    name = "lugorguk"
    allowed_domains = ["lug.org.uk"]
    start_urls = (
        'https://lug.org.uk/listings',
    )

    def parse(self, response):
        for url in response.xpath('//table//a[.="More..."]/@href'):
            yield scrapy.Request(
                response.urljoin(url.extract()),
                self.parse_content)

    def parse_content(self, response):
        il = LugOrgUkItemLoader(selector=response.xpath(
            '//div[@class="left-corner"]'))
        il.add_xpath('name', 'h2/text()')
        il.add_xpath(
            'region', '//strong[text()="Region:"]/following::text()[1]')
        il.add_value('country', u'United Kingdom')
        il.add_xpath('since', '//span[@class="date-display-single"]/text()')
        il.add_xpath(
            'last_update',
            '//strong[text()="Entry last updated:"]/following::text()[1]')
        il.add_xpath(
            'website', '//strong[text()="Website:"]/following::a[1]/@href')
        il.add_xpath(
            'contact',
            '//strong[text()="Contact/LUGmaster:"]/following::a[1]/@href')
        yield il.load_item()

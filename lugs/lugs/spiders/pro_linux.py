# -*- coding: utf-8 -*-

from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider
from scrapy.spiders import Rule

from lugs.loader import ProLinuxItemLoader


class ProLinuxSpider(CrawlSpider):
    name = "pro-linux"
    allowed_domains = ["pro-linux.de"]
    start_urls = (
        'http://www.pro-linux.de/lugs',
    )
    rules = (
        Rule(LinkExtractor(allow=('lugs/1/',))),
        Rule(LinkExtractor(allow=('lugs/2/',)), callback='parse_item')
    )

    def parse_item(self, response):
        il = ProLinuxItemLoader(response=response)
        il.add_xpath('name', '//div[@id="mb"]/h4/text()')
        il.add_xpath('region', '//div[@id="vb"]/div/node()[1]')
        il.add_xpath('country', '//div[@id="vb"]/div/node()[3]')
        il.add_xpath('since', '//div[@id="vb"]/div/node()[5]')
        il.add_xpath('last_update', '//div[@id="vb"]/div/node()[7]')
        il.add_xpath('website', '//div[@id="vb"]/div/a[1]/@href')
        il.add_xpath('contact', '//div[@id="vb"]/div/a[2]/@href')
        yield il.load_item()

# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class LugsItem(scrapy.Item):
    name = scrapy.Field()
    region = scrapy.Field()
    country = scrapy.Field()
    since = scrapy.Field()
    last_update = scrapy.Field()
    website = scrapy.Field()
    contact = scrapy.Field()

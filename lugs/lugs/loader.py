# -*- coding: utf-8 -*-

import re
import datetime

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst
from scrapy.loader.processors import MapCompose

from lugs.items import LugsItem


MONTH_NAMES_DE = [
    u'Jänner', u'Februar', u'März', u'April', u'Mai', u'Juni',
    u'Juli', u'August', u'September', u'Oktober', u'November', u'Dezember']


class LugsItemLoader(ItemLoader):
    default_item_class = LugsItem
    default_output_processor = TakeFirst()

    name_in = MapCompose(unicode.strip, unicode.title)
    region_in = MapCompose(unicode.strip, unicode.title)
    country_in = MapCompose(unicode.strip, unicode.title)
    website_in = MapCompose(unicode.strip)
    contact_in = MapCompose(unicode.strip)


class ProLinuxItemLoader(LugsItemLoader):
    def build_datetime(raw_value):
        # NOTE: avoid changing the locale is it not thread safe.
        mo = re.search(r'\s(\d+)\. (\w+) (\d+)', raw_value)
        if not mo:
            return

        parts = list(mo.groups())
        try:
            parts[0] = int(parts[0])
            parts[1] = MONTH_NAMES_DE.index(parts[1]) + 1
            parts[2] = int(parts[2])
        except ValueError:
            return

        return datetime.datetime(
            year=parts[2], month=parts[1], day=parts[0])

    since_in = MapCompose(unicode.strip, build_datetime)
    last_update_in = MapCompose(unicode.strip, build_datetime)


class LugOrgUkItemLoader(LugsItemLoader):
    def build_datetime_since(raw_value):
        return datetime.datetime.strptime(raw_value, '%Y/%m')

    def build_datetime_last_update(raw_value):
        return datetime.datetime.strptime(raw_value, '%m/%Y')

    since_in = MapCompose(unicode.strip, build_datetime_since)
    last_update_in = MapCompose(unicode.strip, build_datetime_last_update)
